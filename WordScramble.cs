﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Word
{
	public string word;
	[Header("Leave empty if you want randomized")]
	public string desiredRandom;

	public string GetString()
	{
		if (!string.IsNullOrEmpty (desiredRandom)) 
		{
			return desiredRandom;
		}
		
		string result = word;

		while (result == word)
		{
			result = "";
			List<char> characters = new List<char>(word.ToCharArray());
			while (characters.Count > 0)
			{
				int indexChar = Random.Range(0, characters.Count - 1);
				result += characters[indexChar];

				characters.RemoveAt(indexChar);
			}

		}

		return result;
	}
}

public class WordScramble : MonoBehaviour 
{
	public Word[] words;

	[Header("UI Reference")]
	public CharBox prefab;
	public Transform container;
	public float space;

	List<CharBox> charBoxes = new List<CharBox>();
	CharBox firstSelected;

	public int currentWord;
	public static WordScramble main;

	void Awake()
	{
		main = this;
	}

	// Use this for initialization
	void Start () {
		ShowScramble (currentWord);
	}
	
	// Update is called once per frame
	void Update () {
		repositionBox ();
	}

	void repositionBox()
	{
		if (charBoxes.Count == 0)
		{
			return;
		}

		float center = (charBoxes.Count - 1) / 2;
		for (int i = 0; i < charBoxes.Count; i++) {
			charBoxes [i].rectTransform.anchoredPosition = new Vector2 ((i - center) * space, 0);
			charBoxes [i].index = i;
		}
	}

	// Show a random word to the screen
	public void ShowScramble ()
	{
		ShowScramble(Random.Range(0, (words.Length - 1)));
	}

	// Show word from collection with desired index
	public void ShowScramble (int index)
	{
		charBoxes.Clear ();
		foreach (Transform child in container) {
			Destroy (child.gameObject);
		}

		if (index > words.Length - 1) {
			Debug.LogError ("Index out of range, please enter range beetwen 0 -" + (words.Length - 1).ToString ());
			return;
		}

		char[] chars = words [index].GetString ().ToCharArray ();
		foreach (char c in chars) {
			CharBox clone = Instantiate (prefab.gameObject).GetComponent<CharBox> ();
			clone.transform.SetParent (container);

			charBoxes.Add (clone.init (c));
		}

		currentWord = index;
	}
	
	public void swap (int indexA, int indexB)
	{
		CharBox tmpA = charBoxes [indexA];

		charBoxes [indexA] = charBoxes [indexB];
		charBoxes [indexB] = tmpA;

		charBoxes [indexA].transform.SetAsLastSibling ();
		charBoxes [indexB].transform.SetAsLastSibling ();
	}

	public void Select (CharBox charBox)
	{
		if (firstSelected) {
			swap (firstSelected.index, charBox.index);

			// Unselect
			firstSelected = null;
			firstSelected.Select ();
			charBox.Select ();
		}
			else
			{
			firstSelected = charBox;
			}
	}

	public void UnSelect()
	{
		firstSelected = null;
	}

	public bool CheckWord ()
	{
		string word = "";
		foreach (CharBox charBox in charBoxes) {
			word += charBox.character;
		}

		if (word == words[currentWord].word)
		{
			currentWord++; 
			ShowScramble (currentWord);

			return true;
		}
		return false;
	}
}
