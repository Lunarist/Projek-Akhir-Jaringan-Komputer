﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grabableObject : MonoBehaviour
{
    public bool isGrabbing;
    RaycastHit2D hit;
    public Transform holdpoint;
    //public float throwforce;
    public LayerMask notgrabbed;
    float getSpeed = charaMove.moveSpeed; // mengakses fungsi speed di script playerMovement
    float newSpeed = 7.5f; // speed ketika mengangkat barang
    private float distance = 1f;

   // public Sprite moveAndGrab;
    //move and grab disingkan menjadi MAG
    public Sprite magUp;
    public Sprite magDown;
    public Sprite magRight;
    public Sprite magLeft;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // ketika status isGrabbing tidak menyala, maka ketika pemain menekan tombol "Space" maka objek akan terangkat
            if (!isGrabbing)
            {
                Physics2D.queriesStartInColliders = false;
                hit = Physics2D.Raycast(transform.position, Vector3.right * transform.localScale.x, distance);

                if (hit.collider != null && hit.collider.tag == "grabable")
                {
                    isGrabbing = true;
                    charaMove.moveSpeed = newSpeed;
                }
            }
            // ketika status isGrabbing menyala, maka ketika pemain menekan tombol "Space" maka objek akan terlepas
            else if (!Physics2D.OverlapPoint(holdpoint.position, notgrabbed))
            {
                isGrabbing = false;
                charaMove.moveSpeed = 10f;
                if (hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
                {
                    hit.collider.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x, 1 /* * throwforce*/);
                }
            }
        }

        // set object position to holdpoint position
        if (isGrabbing)
        {
           // this.gameObject.GetComponent<SpriteRenderer>().sprite = moveAndGrab;

           /* if (isGrabbing && Input.GetKey(KeyCode.W))
                this.gameObject.GetComponent<SpriteRenderer>().sprite = magUp;

            if (isGrabbing && Input.GetKey(KeyCode.A))
                this.gameObject.GetComponent<SpriteRenderer>().sprite = magLeft;

            if (isGrabbing && Input.GetKey(KeyCode.S))
                this.gameObject.GetComponent<SpriteRenderer>().sprite = magDown;

            if (isGrabbing && Input.GetKey(KeyCode.D))
                this.gameObject.GetComponent<SpriteRenderer>().sprite = magRight; */

            hit.collider.gameObject.transform.position = holdpoint.position;
        }
    }

}
