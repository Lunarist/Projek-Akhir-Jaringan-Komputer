﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class photonHandler : MonoBehaviour
{
    public photonButton photonB;
    public GameObject mainPlayer;

    private void Awake()
    {
        DontDestroyOnLoad(this.transform);

       // PhotonNetwork.sendRate = 30;
       // PhotonNetwork.sendRateOnSerialize = 20;

        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    // fungsi untuk membuat room baru
    public void createNewRoom()
    {
        PhotonNetwork.CreateRoom(photonB.createRoomInput.text, new RoomOptions() { MaxPlayers = 4 }, null);
    }

    // fungsi untuk join/create suatu room
    public void joinOrCreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;
        PhotonNetwork.JoinOrCreateRoom(photonB.joinRoomInput.text, roomOptions, TypedLobby.Default);
    }

    // pindah ke scene game.scene
    public void moveScene()
    {
       // photonB = null;

        PhotonNetwork.LoadLevel("game.scene");
    }

    // transisi scene
    private void OnJoinedRoom()
    {
        moveScene();
        Debug.Log("Connected to the room");
    }

    // fungsi memanggil player ketika berpindah ke scene game.scene
    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "game.scene")
        {
            spawnPlayer();
        }
    }

    // memanggil player 
    private void spawnPlayer()
    {
        PhotonNetwork.Instantiate(mainPlayer.name, mainPlayer.transform.position, mainPlayer.transform.rotation, 0);
    }
}
