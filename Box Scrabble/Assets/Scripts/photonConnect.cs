﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class photonConnect : MonoBehaviour
{
    public string versionName = "1";
    public GameObject sectionView1, sectionView2, sectionView3;

    private void Awake()
    {
        // menghubungkan ke photon
        PhotonNetwork.ConnectUsingSettings(versionName);
        Debug.Log("Connecting to photon ... ");
    }

    private void OnConnectedToMaster()
    {
        // join lobby ketika berada di master server
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("Connected to photon master");
    }

    private void OnJoinedLobby()
    {
        // ketika "join lobby" aktif, matikan sectionView1 dan nyalakan sectionView2
        sectionView1.SetActive(false);
        sectionView2.SetActive(true);

        Debug.Log("On joined lobby");
    }

    private void OnDisconnectedFromPhoton()
    {
        // ketika terputus dari photon, aktifkan sectionView3 dan mematikan yang lainnya
        if (sectionView1.activeSelf)
            sectionView1.SetActive(false);
        if (sectionView2.activeSelf)
            sectionView2.SetActive(false);

        sectionView3.SetActive(true);

        Debug.Log("Disconnected from photon");
    }
}
