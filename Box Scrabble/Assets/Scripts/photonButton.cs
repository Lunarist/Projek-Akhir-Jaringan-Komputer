﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class photonButton : MonoBehaviour
{
    public InputField createRoomInput, joinRoomInput;
    public photonHandler pHandler;

    public void OnClickCreateRoom() // create server
    {
        pHandler.createNewRoom();
    }

    public void OnClickJoinRoom() // join server
    {
        pHandler.joinOrCreateRoom();
    }

   
}
