﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class charaMove : MonoBehaviour
{
    public bool devTesting = false;

    public PhotonView photonView;

    public Text playerText;

    public static float moveSpeed = 10f;
   // public float jumpForce = 800f;

    private Vector3 selfpos ;
   

    public Sprite movingUp, movingRight, movingLeft, movingDown;
    private Vector2 direction;

    public GameObject playerCam;
    private GameObject sceneCam;

    private void Awake()
    {
        if (!devTesting && photonView.isMine)
        {
            sceneCam = GameObject.Find("Main Camera");
            sceneCam.SetActive(false);
            playerCam.SetActive(true);
        }
    }

    private void Update()
    {
        if (!devTesting)
        {
            if (photonView.isMine)
            {
                movement();
                checkInput();
            }

            else
                smoothNetMovement();
        }
        else
        {
            movement();
            checkInput();
        }
    }

    private void movement()
    {
        transform.Translate(direction * moveSpeed * Time.deltaTime);
    }

    private void checkInput()
    {
       // var move = new Vector3(Input.GetAxis("Horizontal"), 0);
       // transform.position += move * moveSpeed * Time.deltaTime;

        direction = Vector2.zero;

        if (Input.GetKey(KeyCode.W))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = movingUp;
            direction += Vector2.up;
        }

        if (Input.GetKey(KeyCode.A))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = movingLeft;
            direction += Vector2.left;
        }

        if (Input.GetKey(KeyCode.S))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = movingDown;
            direction += Vector2.down;
        }

        if (Input.GetKey(KeyCode.D))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = movingRight;
            direction += Vector2.right;
        }
    }

    private void smoothNetMovement()
    {
        transform.position = Vector3.Lerp(transform.position, selfpos, Time.deltaTime * 8);
    }
    
    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
            stream.SendNext(transform.position);
        else
            selfpos = (Vector3)stream.ReceiveNext();
    }
}
