﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
public class sceneTransition : MonoBehaviour
{
    public void moveScene(int sceneLevel)
    {
        SceneManager.LoadScene(sceneLevel);
    }

    public void exitScene()
    {
        Application.Quit();
    }
   
}
