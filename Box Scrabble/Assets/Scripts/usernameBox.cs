﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class usernameBox : MonoBehaviour
{
    private bool alreadyRegistered = false;

    public InputField usernameInput;
    public GameObject objectParrent, enterButton;
    private bool removePrefs;

    private void Awake()
    {
        if (removePrefs)
            PlayerPrefs.SetInt("registered", 0);

        if (PlayerPrefs.GetInt("registered") == 0)
        {

        }

        checkRegister();
    }

    private void checkRegister()
    {
        if (!alreadyRegistered)
            objectParrent.SetActive(true);
    }

    public void usernameInputChange()
    {
        if (usernameInput.text.Length >= 2)
        {
            enterButton.SetActive(true);
        }
        else
        {
            enterButton.SetActive(false);
        }
     }

    public void createUsername()
    {
        PhotonNetwork.playerName = usernameInput.text;
        PlayerPrefs.SetInt("registered", 1);
    }
}
