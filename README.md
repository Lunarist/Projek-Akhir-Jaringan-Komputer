JUDUL GAME : Blindfold

NAMA ANGGOTA :
1. Gunawan Wibisono         (4210171002)
2. Erieca Faradina Putri    (4210171009)
3. Titan Adi Narendra       (4210171017)

PROGRESS :
1. 16/05/2018 - Game Design
2. 18/05/2018 - Player Movement
3. 23/05/2018 - Pick up & Drop off object + update script Player Movement
4. 03/06/2018 - Update grabableObject & playerMovement Script
5. 29/06/2018 - Deklarasi objek sebagai char, dan penambahan game design