// Blindfold
// Progress 18/05/2018
// Player Movement
//
// Updated 23/05/2018
// Updated 03/06/2018

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour 
{ 	
	//[SerializeField] // atribut untuk menampilkan variabel private
	public static float speed = 4f; // update public + static, to be able to access this variable
	private Vector2 direction;

	public Sprite movingW;
	public Sprite movingA;
	public Sprite movingS;
	public Sprite movingD;
	public Sprite movingWD;
	public Sprite movingDS;
	public Sprite movingSA;
	public Sprite movingAW;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		ControlKey ();
		Movement ();
	}

	// pergerakan karakter 
	public void Movement()
	{
		transform.Translate	(direction * speed * Time.deltaTime);
	}

	// key untuk pergerakan karakter (kontrol karakter)
	private void ControlKey()
	{
		direction = Vector2.zero;

		// control karakter ke atas (W)
		if (Input.GetKey(KeyCode.W)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingW;
			direction += Vector2.up;
		}

		// control karakter ke kiri (A)
		if (Input.GetKey(KeyCode.A)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingA;
			direction += Vector2.left;
		}

		// control karakter ke bawah (S)
		if (Input.GetKey(KeyCode.S)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingS;
			direction += Vector2.down;
		}

		// control karakter ke kanan (D)
		if (Input.GetKey(KeyCode.D)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingD;
			direction += Vector2.right;
		}

		// control karakter ke kanan atas (W + D)
		if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingWD;
		}

		// control karakter ke kanan bawah (D + S)
		if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingDS;
		}

		// control karakter ke kiri bawah (S + A)
		if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingSA;
		}

		// control karakter ke kiri atas (A + W)
		if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W)) 
		{
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = movingAW;
		}
	}
}
